# CentOS Stream 9 Docker images

http://cern.ch/linux/centos9/

## What is CentOS Stream 9 (CS9)?

Upstream CentOS Stream 9, with minimal patches to integrate into the CERN computing environment

### Image building

```
koji image-build cs9-docker-base `date "+%Y%m%d"` cs9-image-9x \
	http://linuxsoft.cern.ch/cern/centos/s9/BaseOS/\$arch/os/ x86_64 aarch64 \
	--ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/cs9-base#master \
	--kickstart=cs9-base-docker.ks --ksversion=RHEL8 --distro=RHEL-8.2 --format=docker \
	--factory-parameter=dockerversion 1.10.1 \
	--factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
	--factory-parameter=docker_cmd '["/bin/bash"]' \
	--factory-parameter=generate_icicle False \
	--wait --scratch
```
